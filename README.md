# docker-base-ssl #

[![build status](https://gitlab.com/andrewheberle/docker-base-ssl/badges/master/build.svg)](https://gitlab.com/andrewheberle/docker-base-ssl/commits/master)

## Description ##

Base image derived from "registry.gitlab.com/andrewheberle/docker-base" with SSL certificate generation built in.

## SSL Certificate Generation ##

Has OpenSSL installed and supports three options for SSL certificates:

 - Manual
 - Self Signed
 - Lets Encrypt

