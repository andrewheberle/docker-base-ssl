FROM registry.gitlab.com/andrewheberle/docker-base:1.5

MAINTAINER Andrew Heberle (https://gitlab.com/andrewheberle/docker-base-ssl)

# Install packages
RUN apk --no-cache add curl \
    openssl \
    ca-certificates && \
    curl https://get.acme.sh | sh && \
    # Grab LetsEncrypt CA certficates
    cd /etc/ssl/certs && curl -O https://letsencrypt.org/certs/isrgrootx1.pem && \
    cd /etc/ssl/certs && curl -O https://letsencrypt.org/certs/letsencryptauthorityx3.pem && \
    update-ca-certificates

# Environment
ENV SSL_MODE=selfsigned \
    SSL_CERT=/etc/ssl/server.crt \
    SSL_KEY=/etc/ssl/server.key \
    SSL_PEM=/etc/ssl/server.pem \
    SSL_CA=/etc/ssl/ca.crt \
    SSL_CA_CHAIN=/etc/ssl/ca.pem \
    SSL_REQ_CN=domain.example.net \ 
    SSL_REQ_C=AU \
    SSL_REQ_ST=Some-State \
    SSL_REQ_L="Some Locality" \
    SSL_REQ_O="Some Org" \
    SSL_REQ_OU="Some OU" \
    SSL_REQ_emailAddress="ssl@example.net" \
    SSL_REQ_BITS=2048 \
    SSL_PRIVATE_MODE=0640 \
    SSL_PUBLIC_MODE=0644 \
    SSL_USER=root \
    SSL_GROUP=ssl \
    ACME_OPTS= \
    ACME_DNS_API=cf \
    ACME_DNS_API_USERNAME="user@example.net" \
    ACME_DNS_API_PASSWORD="XXXXXXXXXXXXXXXXXX"

# Copy root fs skeleton
COPY root /

ENTRYPOINT ["/init"]
CMD []

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL name="docker-base" \
      version="$VERSION" \
      architecture="amd64" \
      org.label-schema.description="Base image from registry.gitlab.com/andrewheberle/docker-base with SSL generation support" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="docker-base" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="https://gitlab.com/andrewheberle/docker-base-ssl" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"
